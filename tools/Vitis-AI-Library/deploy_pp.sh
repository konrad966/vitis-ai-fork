#/usr/bin/env bash

# LOCAL_PATH=/home/konradl/Xilinx/petalinux/2020.2/sysroots/aarch64-xilinx-linux/usr/lib/libvitis_ai_library-pointpillars.so.1.3.2
LOCAL_PATH=/home/konradl/Xilinx/petalinux/2020.2/sysroots/aarch64-xilinx-linux/install/Debug/lib/libvitis_ai_library-pointpillars.so.1.3.2
REMOTE_PATH=/usr/lib/libvitis_ai_library-pointpillars.so.1.3.1
sshpass -p root scp ${LOCAL_PATH} root@192.168.2.99:${REMOTE_PATH}

LOCAL_PATH=/home/konradl/Xilinx/petalinux/2020.2/sysroots/aarch64-xilinx-linux/install/Debug/include/vitis/ai/pointpillars.hpp
REMOTE_PATH=/usr/include/vitis/ai/pointpillars.hpp
sshpass -p root scp ${LOCAL_PATH} root@192.168.2.99:${REMOTE_PATH}