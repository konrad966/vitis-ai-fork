#pragma once

#include <vector>
#include <string>
#include "types.hpp"

void multithreshold(const std::vector<PFN_T> &in, std::vector<PFN_QUANTIZED_T> &out);

class PFN {
    private:
        std::vector<PFN_T> weights;
        std::vector<PFN_T> bn_mean;
        std::vector<PFN_T> bn_var;
        std::vector<PFN_T> bn_gamma;
        std::vector<PFN_T> bn_beta;
        PFN_T bn_eps;
        int in_width;
        int in_height;
        int in_channels;
        int out_width;
        int out_height;
        int out_channels;
    public:
        PFN(
            char* weights_filename,
            char* bn_mean_filename,
            char* bn_var_filename,
            char* bn_gamma_filename,
            char* bn_beta_filename,
            int in_width,
            int in_height,
            int in_channels,
            int out_width,
            int out_height,
            int out_channels
        );
        void execute_pixel(
            const PFN_T * in_channels,
            PFN_T * out_channels
        );
        void execute(const std::vector<PFN_T>& input, std::vector<PFN_T>& output);
};