#pragma once

#include <vector>
#include <algorithm>
#include <cstdint>
#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#include <iterator>
#include "types.hpp"
#include <cmath>

void load_vector(char* filename, std::vector<PFN_T>& buffer);
