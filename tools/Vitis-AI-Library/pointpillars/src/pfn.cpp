#include <vector>
#include <algorithm>
#include <cstdint>
#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#include <iterator>
#include "pfn.hpp"
#include "types.hpp"
#include <cmath>
#include "utils.hpp"


void multithreshold(const std::vector<PFN_T> &in, std::vector<PFN_QUANTIZED_T> &out) {
    constexpr float range_beg = 0.0;
    constexpr float range_end = 6.0;
    constexpr int level_num = 256;
    constexpr float divider = (range_end - range_beg) / level_num;
    
    std::fill(out.begin(), out.end(), 0);
    
    for (size_t i=0; i<in.size(); i++) {
        out[i] = static_cast<PFN_QUANTIZED_T>(fmax(
            fmin(
                in[i] / divider, 
                level_num - 1
            ),
            0
        ));
    }
}

PFN::PFN(
    char* weights_filename,
    char* bn_mean_filename,
    char* bn_var_filename,
    char* bn_gamma_filename,
    char* bn_beta_filename,
    int in_width,
    int in_height,
    int in_channels,
    int out_width,
    int out_height,
    int out_channels
) :
    in_width(in_width),
    in_height(in_height),
    in_channels(in_channels),
    out_width(out_width),
    out_height(out_height),
    out_channels(out_channels)
 {
    if(in_height != out_height) {
        std::cout << "in_width should be the same as out_width, but they are equal to " << in_width << " and " << out_width << " respecively" << std::endl;
    }

    load_vector(weights_filename, this->weights);
    size_t expected_size = this->in_channels * this->out_channels;
    if (expected_size != this->weights.size()) {
        std::cout << "Wrong weights file, expected size: " << expected_size << ", but got the following: " << this->weights.size() << std::endl;
    }

    load_vector(bn_mean_filename, this->bn_mean);
    if (size_t(this->out_channels) != this->bn_mean.size()) {
        std::cout << "Wrong bn_mean file, expected size: " << this->out_channels << ", but got the following: " << this->bn_mean.size() << std::endl;
    }
    load_vector(bn_var_filename, this->bn_var);
    if (size_t(this->out_channels) != this->bn_var.size()) {
        std::cout << "Wrong bn_var file, expected size: " << this->out_channels << ", but got the following: " << this->bn_var.size() << std::endl;
    }
    load_vector(bn_gamma_filename, this->bn_gamma);
    if (size_t(this->out_channels) != this->bn_gamma.size()) {
        std::cout << "Wrong bn_gamma file, expected size: " << this->out_channels << ", but got the following: " << this->bn_gamma.size() << std::endl;
    }
    load_vector(bn_beta_filename, this->bn_beta);
    if (size_t(this->out_channels) != this->bn_beta.size()) {
        std::cout << "Wrong bn_beta file, expected size: " << this->out_channels << ", but got the following: " << this->bn_beta.size() << std::endl;
    }
}

void PFN::execute_pixel(
    const PFN_T * in,
    PFN_T * out
) {
    // Scalar multiply + BN
    for (int i = 0; i < this->out_channels; i++) {
        // out[i] = 0; // We don't need to do this,
        // the whole output vector was zeroed before 
        // Scalar mul
        for (int j = 0; j < this->in_channels; j++) {
            out[i] += in[j] * this->weights[i*this->in_channels + j]; //scalar
        }
        // BN
        out[i] = (out[i] - this->bn_mean[i]) / sqrt(this->bn_var[i] + this->bn_eps) * this->bn_gamma[i] + this->bn_beta[i];
    }

}

void PFN::execute(const std::vector<PFN_T>& input, std::vector<PFN_T>& output) {
    std::vector<PFN_T> tmp_out(this->in_width * this->out_channels);
    std::fill(output.begin(), output.end(), 0);
    for (int i = 0; i < this->in_height; i++) {
        std::fill(tmp_out.begin(), tmp_out.end(), 0);
        for (int j = 0; j < this->in_width; j++) {
            int elem_num = i * in_width + j;
            this->execute_pixel(
                &(input[elem_num*this->in_channels]),
                &(tmp_out[j*this->out_channels])
                // &(output[elem_num*this->out_channels])
            );
            for (int c = 0; c < this->out_channels; c++) {
                if (tmp_out[j*this->out_channels + c] > output[elem_num*this->out_channels + c]) {
                    output[elem_num*this->out_channels + c] = tmp_out[j*this->out_channels + c];
                }
                else {
                }
            }
        }
        // ReLU is implemented in an indirect way
        //  the output is first filled with zeros
        //  then in every "pixel" per-channel values are compared, looking for maximum
        //  if the maximum is greater than zero, it does not have to be ReLU'ed (as ReLU does not change positive values)
        //  if the maximum is lower than zero, no change will be done as output is already zero
        //    because it was zeroed at the beginning and zero is greater than all the negative values,
        //    and the negative values would have been changed to zeros with ReLU anyway
    }
}
